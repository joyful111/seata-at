package cn.tedu.storage;


import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DSAutoConfiguration {
    //创建原始的数据源
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource hikariDS(){
        //hikari使用的数据库链接地址，使用jdbcURL属性
        return new HikariDataSource();
    }

    //创建seata at 代理数据源
    @Bean
    @Primary //首选对象
    public DataSource dataSource(DataSource hikariDs){
        //hikariDS是被代理的目标对象
        return new DataSourceProxy(hikariDs);
    }

}
